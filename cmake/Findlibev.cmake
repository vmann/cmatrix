find_path(libev_INCLUDE_DIR ev.h)
mark_as_advanced(libev_INCLUDE_DIR)

find_library(libev_LIBRARY ev)
mark_as_advanced(libev_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  libev DEFAULT_MSG libev_LIBRARY libev_INCLUDE_DIR)

if(libev_FOUND AND NOT TARGET libev::ev)
  add_library(libev::ev SHARED IMPORTED)
  set_target_properties(libev::ev PROPERTIES
    IMPORTED_LOCATION ${libev_LIBRARY}
    INTERFACE_INCLUDE_DIRECTORIES ${libev_INCLUDE_DIR})
  #  target_compile_definitions(libev::ev
  #    INTERFACE
  #      EV_COMPAT3=0
  #      EV_MULTIPLICITY=1)
endif()
