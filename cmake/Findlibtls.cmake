find_path(libtls_INCLUDE_DIR tls.h)
mark_as_advanced(libtls_INCLUDE_DIR)

find_library(libtls_LIBRARY tls)
mark_as_advanced(libtls_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  libtls DEFAULT_MSG libtls_LIBRARY libtls_INCLUDE_DIR)

if(libtls_FOUND AND NOT TARGET libtls::tls)
  add_library(libtls::tls SHARED IMPORTED)
  set_target_properties(libtls::tls PROPERTIES
    IMPORTED_LOCATION ${libtls_LIBRARY}
    INTERFACE_INCLUDE_DIRECTORIES ${libtls_INCLUDE_DIR})
endif()
