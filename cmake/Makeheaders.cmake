find_program(makeheaders_EXECUTABLE makeheaders)
mark_as_advanced(makeheaders_EXECUTABLE)

if(makeheaders_EXECUTABLE)
  add_executable(makeheaders IMPORTED)
  set_target_properties(makeheaders PROPERTIES
    IMPORTED_LOCATION ${makeheaders_EXECUTABLE})
else()
  add_executable(makeheaders ${CMAKE_CURRENT_LIST_DIR}/makeheaders.c)
endif()

unset(_MAKEHEADERS)

function(target_makeheaders target)
  get_target_property(binary_dir ${target} BINARY_DIR)
  get_target_property(source_dir ${target} SOURCE_DIR)
  get_target_property(sources ${target} SOURCES)
  foreach(source ${sources})
    cmake_path(REPLACE_EXTENSION source h OUTPUT_VARIABLE header)
    cmake_path(ABSOLUTE_PATH source
      BASE_DIRECTORY ${source_dir}
      NORMALIZE)
    list(APPEND makeheader_args ${source}:${header})
    cmake_path(ABSOLUTE_PATH header
      BASE_DIRECTORY ${binary_dir}
      NORMALIZE)
    list(APPEND generated_headers ${header})
  endforeach()
  add_custom_command(
    OUTPUT ${generated_headers}
    COMMAND makeheaders -local -- ${makeheader_args}
    DEPENDS ${sources}
    COMMENT "Generating headers for target ${target}"
    VERBATIM)
  target_sources(${target} PRIVATE ${generated_headers})
  target_include_directories(${target} PRIVATE ${binary_dir})
endfunction()
