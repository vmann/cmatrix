#include <buffer.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#if INTERFACE

#include <stdlib.h>

struct Buffer {
  size_t size;
  char data[4096];
};

#define buffer_size(buffer) ((buffer)->size)
#define buffer_capacity(buffer) (sizeof((buffer)->data))
#define buffer_data(buffer) ((buffer)->data)

#endif

void buffer_consume(Buffer *buffer, size_t n) {
  buffer->size -= n;
  memmove(buffer->data, &buffer->data[n], buffer->size);
}

void buffer_provide(Buffer *buffer, size_t n) {
  buffer->size += n;
}

int buffer_printf(Buffer *buffer, const char *format, ...) {
  va_list ap;
  int ret;

  va_start(ap, format);
  ret = vsnprintf(buffer_data_remaining(buffer), buffer_size_remaining(buffer),
                  format, ap);
  if (ret > 0) {
    if ((size_t)ret < buffer_size_remaining(buffer)) {
      buffer_provide(buffer, ret);
    } else {
      buffer_provide(buffer, buffer_size_remaining(buffer));
    }
  }
  va_end(ap);
  return ret;
}

size_t buffer_size_remaining(const Buffer *buffer) {
  return buffer_capacity(buffer) - buffer_size(buffer);
}

char *buffer_data_remaining(Buffer *buffer) {
  return &buffer->data[buffer_size(buffer)];
}
