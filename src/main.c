#include <ev.h>
#include <main.h>
#include <tls.h>

int main(int argc, char **argv) {
  Http http;

  (void)argc, (void)argv;
  tls_init();
  if (http_get(EV_DEFAULT_ & http, "chat.heizhaus.org",
               "/.well-known/matrix/client")) {
    return 1;
  }
  ev_run(EV_DEFAULT_UC_ 0);
  return 0;
}
